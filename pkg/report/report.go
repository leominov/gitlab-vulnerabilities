package report

import (
	"encoding/json"
	"strings"
)

// ref: https://docs.gitlab.com/ee/user/application_security/container_scanning/#reports-json-format
// ref: https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/container-scanning-report-format.json
type Report struct {
	Version         string           `json:"version"`
	Scan            *Scan            `json:"scan"`
	Schema          string           `json:"schema"`
	Vulnerabilities []*Vulnerability `json:"vulnerabilities"`
	Remediations    []*Remediation   `json:"remediations"`
}

type Scan struct {
	EndTime   string     `json:"end_time"`
	Messages  []*Message `json:"messages"`
	StartTime string     `json:"start_time"`
	// success, failure
	Status string `json:"status"`
	// container_scanning, dependency_scanning, dast, sast
	Type string `json:"type"`
}

type Message struct {
	// info, warn, fatal
	Level   string   `json:"level"`
	Value   string   `json:"value"`
	Scanner *Scanner `json:"scanner"`
}

type Scanner struct {
	ID      string  `json:"id"`
	Name    string  `json:"name"`
	URL     string  `json:"url"`
	Version string  `json:"version"`
	Vendor  *Vendor `json:"vendor"`
}

type Vendor struct {
	Name string `json:"name"`
}

type Vulnerability struct {
	ID          string `json:"id"`
	Category    string `json:"category"`
	Name        string `json:"name"`
	Message     string `json:"message"`
	Description string `json:"description"`
	// Deprecated, use id instead
	CVE string `json:"cve"`
	// Info, Unknown, Low, Medium, High, Critical
	Severity string `json:"severity"`
	// Ignore, Unknown, Experimental, Low, Medium, High, Confirmed
	Confidence  string        `json:"confidence"`
	Solution    string        `json:"solution"`
	Scanner     *ScannerShort `json:"scanner"`
	Identifiers []*Identifier `json:"identifiers"`
	Links       []*Link       `json:"links"`
	Location    *Location     `json:"location"`
}

type ScannerShort struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Identifier struct {
	Type  string `json:"type"`
	Name  string `json:"name"`
	URL   string `json:"url"`
	Value string `json:"value"`
}

type Link struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

type Location struct {
	Dependency      *Dependency `json:"dependency"`
	OperatingSystem string      `json:"operating_system"`
	Image           string      `json:"image"`
}

type Dependency struct {
	Package *Package `json:"package"`
	Version string   `json:"version"`
}

type Package struct {
	Name string `json:"name"`
}

type Remediation struct {
	Fixes   []*Fix `json:"fixes"`
	Summary string `json:"summary"`
	Diff    string `json:"diff"`
}

type Fix struct {
	ID string `json:"id"`
	// Deprecated, use id instead
	CVE string `json:"cve"`
}

func (r *Report) String() string {
	out, _ := json.Marshal(r)
	return string(out)
}

func (r *Report) HasVulnerabilities() bool {
	for _, v := range r.Vulnerabilities {
		if strings.ToLower(v.Confidence) == "ignore" {
			continue
		}
		if v.Severity != "Critical" {
			continue
		}
		return true
	}
	return false
}
