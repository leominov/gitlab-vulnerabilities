package client

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"

	"github.com/hashicorp/go-cleanhttp"
)

type Client struct {
	debug    bool
	cli      *http.Client
	endpoint string
	f        *os.File
	vals     url.Values
}

func New(config *Config) (*Client, error) {
	cli := &Client{
		debug:    config.Debug,
		cli:      cleanhttp.DefaultClient(),
		endpoint: config.Endpoint,
	}

	f, err := os.Open(config.Filename)
	if err != nil {
		return nil, err
	}
	cli.f = f

	vals, err := config.Options.Encode()
	if err != nil {
		return nil, err
	}
	cli.vals = vals

	return cli, nil
}

func (c *Client) Report() error {
	req, err := http.NewRequest(http.MethodPost, c.endpoint+"?"+c.vals.Encode(), c.f)
	if err != nil {
		return err
	}

	if c.debug {
		b, _ := httputil.DumpRequestOut(req, true)
		fmt.Println(string(b))
	}

	resp, err := c.cli.Do(req)
	if err != nil {
		return err
	}

	if c.debug {
		b, _ := httputil.DumpResponse(resp, true)
		fmt.Println(string(b))
	}

	if resp.StatusCode == http.StatusOK {
		return nil
	}

	return fmt.Errorf("unexpected response code: %d", resp.StatusCode)
}
