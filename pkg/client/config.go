package client

import (
	"os"

	"github.com/leominov/gitlab-vulnerabilities/pkg/options"
)

type Config struct {
	*options.Options
	Debug    bool
	Endpoint string
	Filename string
}

func LoadConfigFromEnv() (*Config, error) {
	c := &Config{
		Endpoint: os.Getenv("GITLAB_VULNERABILITIES_ENDPOINT"),
		Filename: os.Getenv("GITLAB_VULNERABILITIES_REPORT"),
	}
	opts, err := options.LoadFromEnv()
	if err != nil {
		return nil, err
	}
	c.Options = opts
	return c, nil
}
