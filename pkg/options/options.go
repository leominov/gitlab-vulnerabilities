package options

import (
	"net/http"
	"net/url"

	"github.com/gorilla/schema"
	"github.com/kelseyhightower/envconfig"
)

var (
	decoder = schema.NewDecoder()
	encoder = schema.NewEncoder()
)

type Options struct {
	CommitSha   string `schema:"commit_sha,required" envconfig:"CI_COMMIT_SHA" required:"true"`     // CI_COMMIT_SHA
	JobURL      string `schema:"job_url" envconfig:"CI_JOB_URL" required:"true"`                    // CI_JOB_URL
	PipelineURL string `schema:"pipeline_url" envconfig:"CI_PIPELINE_URL" required:"true"`          // CI_PIPELINE_URL
	ProjectPath string `schema:"project_path,required" envconfig:"CI_PROJECT_PATH" required:"true"` // CI_PROJECT_PATH
	RefName     string `schema:"ref_name,required" envconfig:"CI_COMMIT_REF_NAME" required:"true"`  // CI_COMMIT_REF_NAME
}

func LoadFromEnv() (*Options, error) {
	c := &Options{}
	return c, envconfig.Process("", c)
}

func LoadFromRequest(r *http.Request) (*Options, error) {
	o := &Options{}
	err := decoder.Decode(o, r.URL.Query())
	return o, err
}

func (o *Options) Encode() (url.Values, error) {
	vals := make(map[string][]string)
	err := encoder.Encode(o, vals)
	return vals, err
}
