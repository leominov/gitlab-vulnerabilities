package server

import (
	"encoding/json"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	GitlabEndpoint           string `envconfig:"GITLAB_ENDPOINT" required:"true" default:"https://gitlab.com/api/v4"`
	GitlabToken              string `envconfig:"GITLAB_TOKEN"`
	GitlabUsername           string `envconfig:"GITLAB_USERNAME"`
	GitlabPassword           string `envconfig:"GITLAB_PASSWORD"`
	GitlabReportCommit       bool   `envconfig:"GITLAB_REPORT_COMMIT" default:"true"`
	GitlabReportMergeRequest bool   `envconfig:"GITLAB_REPORT_MERGE_REQUEST" default:"false"`
}

func LoadConfigFromEnv() (*Config, error) {
	c := &Config{}
	return c, envconfig.Process("", c)
}

func (c *Config) String() string {
	b, _ := json.Marshal(c)
	return string(b)
}
