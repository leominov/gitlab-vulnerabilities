package server

import "github.com/xanzy/go-gitlab"

type gitlabClientFake struct {
	mrErr      error
	notesErr   error
	notes      map[interface{}]map[int]*gitlab.Note
	commitsErr error
	commits    map[interface{}]map[string]*gitlab.CommitStatus
}

func newFakeClient() *gitlabClientFake {
	return &gitlabClientFake{
		notes:   make(map[interface{}]map[int]*gitlab.Note),
		commits: make(map[interface{}]map[string]*gitlab.CommitStatus),
	}
}

func (g *gitlabClientFake) CreateMergeRequestNote(pid interface{}, mergeRequest int, opt *gitlab.CreateMergeRequestNoteOptions, options ...gitlab.RequestOptionFunc) (*gitlab.Note, *gitlab.Response, error) {
	note := &gitlab.Note{
		ID:   len(g.notes),
		Body: *opt.Body,
	}
	if _, ok := g.notes[pid]; !ok {
		g.notes[pid] = make(map[int]*gitlab.Note)
	}
	g.notes[pid][mergeRequest] = note
	return note, nil, g.notesErr
}

func (g *gitlabClientFake) ListProjectMergeRequests(pid interface{}, opt *gitlab.ListProjectMergeRequestsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.MergeRequest, *gitlab.Response, error) {
	mrs := []*gitlab.MergeRequest{
		{
			ID: 1,
		},
	}
	return mrs, nil, g.mrErr
}

func (g *gitlabClientFake) SetCommitStatus(pid interface{}, sha string, opt *gitlab.SetCommitStatusOptions, options ...gitlab.RequestOptionFunc) (*gitlab.CommitStatus, *gitlab.Response, error) {
	var (
		ref, name, targetURL, desc string
	)
	if opt.Ref != nil {
		ref = *opt.Ref
	}
	if opt.Name != nil {
		name = *opt.Name
	}
	if opt.TargetURL != nil {
		targetURL = *opt.TargetURL
	}
	if opt.Description != nil {
		desc = *opt.Description
	}
	commitStatus := &gitlab.CommitStatus{
		ID:          1,
		SHA:         sha,
		Ref:         ref,
		Status:      string(opt.State),
		Name:        name,
		TargetURL:   targetURL,
		Description: desc,
	}
	if _, ok := g.commits[pid]; !ok {
		g.commits[pid] = make(map[string]*gitlab.CommitStatus)
	}
	g.commits[pid][sha] = commitStatus
	return nil, nil, g.commitsErr
}
