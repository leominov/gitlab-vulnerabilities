package server

import "github.com/xanzy/go-gitlab"

type gitlabClientReal struct {
	cli *gitlab.Client
}

func (g *gitlabClientReal) CreateMergeRequestNote(pid interface{}, mergeRequest int, opt *gitlab.CreateMergeRequestNoteOptions, options ...gitlab.RequestOptionFunc) (*gitlab.Note, *gitlab.Response, error) {
	return g.cli.Notes.CreateMergeRequestNote(pid, mergeRequest, opt, options...)
}

func (g *gitlabClientReal) ListProjectMergeRequests(pid interface{}, opt *gitlab.ListProjectMergeRequestsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.MergeRequest, *gitlab.Response, error) {
	return g.cli.MergeRequests.ListProjectMergeRequests(pid, opt, options...)
}

func (g *gitlabClientReal) SetCommitStatus(pid interface{}, sha string, opt *gitlab.SetCommitStatusOptions, options ...gitlab.RequestOptionFunc) (*gitlab.CommitStatus, *gitlab.Response, error) {
	return g.cli.Commits.SetCommitStatus(pid, sha, opt, options...)
}
