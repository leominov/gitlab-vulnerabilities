package server

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/leominov/gitlab-vulnerabilities/pkg/client"
	"github.com/leominov/gitlab-vulnerabilities/pkg/options"
)

func TestServer_Handler(t *testing.T) {
	sc := &Config{
		GitlabReportMergeRequest: true,
		GitlabReportCommit:       true,
	}
	g := newFakeClient()
	s := &Server{
		config:    sc,
		gitlabCli: g,
	}

	server := httptest.NewServer(http.HandlerFunc(s.Handler))
	defer server.Close()

	cc := &client.Config{
		Options: &options.Options{
			CommitSha:   "CommitSha",
			JobURL:      "JobURL",
			PipelineURL: "PipelineURL",
			ProjectPath: "ProjectPath",
			RefName:     "RefName",
		},
		Endpoint: server.URL,
		Filename: "../../test_data/example.json",
	}
	c, err := client.New(cc)
	if !assert.NoError(t, err) {
		return
	}

	err = c.Report()
	assert.NoError(t, err)

	notes, ok := g.commits["ProjectPath"]
	if !assert.True(t, ok) {
		return
	}

	status, ok := notes["CommitSha"]
	if !assert.True(t, ok) {
		return
	}

	assert.Equal(t, "Vulnerabilities", status.Name)
	assert.Equal(t, "success", status.Status)
}
