package server

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"

	"github.com/leominov/gitlab-vulnerabilities/pkg/options"
	"github.com/leominov/gitlab-vulnerabilities/pkg/report"
)

type Server struct {
	config    *Config
	gitlabCli gitlabClient
}

type Request struct {
	Options *options.Options
	Report  *report.Report
}

func New(config *Config) (*Server, error) {
	s := &Server{
		config: config,
	}
	return s, s.setupGitLabClient()
}

func (s *Server) setupGitLabClient() error {
	var (
		cli *gitlab.Client
		err error
	)
	if len(s.config.GitlabToken) > 0 {
		cli, err = gitlab.NewClient(s.config.GitlabToken, gitlab.WithBaseURL(s.config.GitlabEndpoint))
	} else {
		cli, err = gitlab.NewBasicAuthClient(s.config.GitlabUsername, s.config.GitlabPassword, gitlab.WithBaseURL(s.config.GitlabEndpoint))
	}
	s.gitlabCli = &gitlabClientReal{
		cli: cli,
	}
	return err
}

func demuxReport(r *http.Request) (*Request, error) {
	o, err := options.LoadFromRequest(r)
	if err != nil {
		return nil, err
	}
	var reports []*report.Report
	err = json.NewDecoder(r.Body).Decode(&reports)
	if err != nil {
		return nil, err
	}
	reportRequest := &Request{
		Report:  reports[0],
		Options: o,
	}
	return reportRequest, err
}

func (s *Server) reportBuildStatus(r *Request) {
	opts := &gitlab.SetCommitStatusOptions{
		State:       gitlab.Success,
		Name:        gitlab.String("Vulnerabilities"),
		Description: gitlab.String("No Vulnerabilities"),
	}
	if r.Report.HasVulnerabilities() {
		opts.State = gitlab.Failed
		opts.Description = gitlab.String("Found Vulnerabilities")
	}
	_, _, err := s.gitlabCli.SetCommitStatus(r.Options.ProjectPath, r.Options.CommitSha, opts)
	if err != nil {
		logrus.
			WithFields(logrus.Fields{
				"project": r.Options.ProjectPath,
				"commit":  r.Options.CommitSha,
				"state":   string(opts.State),
			}).
			WithError(err).
			Error("Failed to set commit status")
	}
}

func (s *Server) processReportRequest(r *Request) error {
	if r.Options == nil || r.Report == nil {
		return nil
	}
	if s.config.GitlabReportCommit {
		s.reportBuildStatus(r)
	}

	if !s.config.GitlabReportMergeRequest {
		return nil
	}
	opts := &gitlab.ListProjectMergeRequestsOptions{
		State:        gitlab.String("opened"),
		SourceBranch: gitlab.String(r.Options.RefName),
	}
	mrs, _, err := s.gitlabCli.ListProjectMergeRequests(r.Options.ProjectPath, opts)
	if err != nil {
		return err
	}
	if len(mrs) == 0 {
		return nil
	}
	return nil
}

func (s *Server) Handler(w http.ResponseWriter, r *http.Request) {
	reportReq, err := demuxReport(r)
	if err != nil {
		logrus.WithError(err).Error("Failed to decode report")
		http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		return
	}
	logrus.Debugf("Incoming report: %v", reportReq.Report)
	err = s.processReportRequest(reportReq)
	if err != nil {
		logrus.WithError(err).Error("Failed to process report")
		http.Error(w, "Failed to process report", http.StatusInternalServerError)
		return
	}
	w.Write([]byte("ok"))
}
