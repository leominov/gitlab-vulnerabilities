module github.com/leominov/gitlab-vulnerabilities

go 1.14

require (
	github.com/gorilla/schema v1.1.0
	github.com/hashicorp/go-cleanhttp v0.5.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/prometheus/client_golang v1.7.1
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.6.1
	github.com/xanzy/go-gitlab v0.32.1
)
