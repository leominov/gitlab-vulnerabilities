package main

import (
	"flag"

	"github.com/sirupsen/logrus"

	"github.com/leominov/gitlab-vulnerabilities/pkg/client"
)

func main() {
	flag.Parse()

	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors: true,
	})

	config, err := client.LoadConfigFromEnv()
	if err != nil {
		logrus.Fatal(err)
	}

	cli, err := client.New(config)
	if err != nil {
		logrus.Fatal(err)
	}

	err = cli.Report()
	if err != nil {
		logrus.Fatalf("Failed: %v", err)
	}
}
