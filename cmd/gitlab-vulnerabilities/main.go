package main

import (
	"flag"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"

	"github.com/leominov/gitlab-vulnerabilities/pkg/server"
)

var (
	listenAddress = flag.String("web.listen-address", ":9201", "Address to listen on for web interface and telemetry.")
	metricsPath   = flag.String("web.telemetry-path", "/metrics", "Path under which to expose metrics.")
	logLevel      = flag.String("log-level", "INFO", "Level of logging.")
)

func main() {
	flag.Parse()

	logrus.SetFormatter(&logrus.JSONFormatter{})
	if level, err := logrus.ParseLevel(*logLevel); err == nil {
		logrus.SetLevel(level)
	}

	config, err := server.LoadConfigFromEnv()
	if err != nil {
		logrus.WithError(err).Fatal("Failed to load configuration")
	}

	srv, err := server.New(config)
	if err != nil {
		logrus.WithError(err).Fatal("Failed to init server")
	}

	logrus.Infoln("Staging gitlab-vulnerabilities...")
	logrus.Debugf("Configuration: %s", config.String())

	logrus.Infof("Listening on address: %s", *listenAddress)
	http.HandleFunc("/report", srv.Handler)
	http.Handle(*metricsPath, promhttp.Handler())

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
             <head><title>GitLab Vulnerabilities</title></head>
             <body>
             <h1>GitLab Vulnerabilities</h1>
             <p><a href='` + *metricsPath + `'>Metrics</a></p>
             <p><a href='/report'>Report</a></p>
             </body>
             </html>`))
	})

	go func() {
		if err := http.ListenAndServe(*listenAddress, nil); err != nil {
			logrus.WithError(err).Fatal("Error starting HTTP server")
		}
	}()

	signCh := make(chan os.Signal, 1)
	signal.Notify(signCh, os.Interrupt, syscall.SIGTERM)
	<-signCh

	logrus.Infoln("Bye")
}
